<?php

namespace Korko\Encrypter;

use RuntimeException;
use Illuminate\Encryption\Encrypter;

class SymmetricalEncrypter extends Encrypter
{
    public function setKey(string $key)
    {
        if (static::supported($key, $this->cipher)) {
            $this->key = $key;
        } else {
            throw new RuntimeException('The only supported ciphers are AES-128-CBC and AES-256-CBC with the correct key lengths.');
        }
    }

    public static function generateKey($cipher = 'AES-128-CBC')
    {
        return parent::generateKey($cipher);
    }

    public function split($payload)
    {
        return $this->getJsonPayload($payload);
    }
}
